package src;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.shape.Rectangle;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import java.util.Scanner;

public class rectangle extends Application {

	@Override
	public void start(Stage primaryStage) {
		// TODO Auto-generated method stub
		//Initializes the scanner
		Scanner scan = new Scanner(System.in);
		//Creates new pane
		Pane pane = new Pane();
		VBox vBox = new VBox(20);
		//Creates new padding
		vBox.setPadding(new Insets(10, 5, 5, 10));
		//Prints the requirements for the rectangle
		System.out.println(
				"The following data will be for Rectangle 1\nWhats the center coordinate, followed by the width and height of the rectangle? ");
		//Strips the input and adds it to the string array
		String[] rect1String = (scan.nextLine()).split("[ ]");
		Rectangle rectangle1 = getRect(rect1String);
		//Prints the requirements for the rectangle
		System.out.println(
				"The following data will be for Rectangle 2\nWhats the center coordinate, followed by the width and height of the rectangle? ");
		String[] rect2String = scan.nextLine().split("[ ]");
		Rectangle rectangle2 = getRect(rect2String);
		//Initializes the string
		String rectangleOutput = "";
		//If rectangle two is contained within rectangle 1
		if (contains(rectangle1, rectangle2) || contains(rectangle2, rectangle1)) 
			rectangleOutput += "One rectangle is contained within another ";
		//If they overlap
		else if (overlaps(rectangle1, rectangle2) || overlaps(rectangle2, rectangle1)) 
			rectangleOutput += "The rectangles overlap ";
		else
			//If they dont overlap
			rectangleOutput += "The rectangle do not overlap";
		//Adds the rectangle to the pane
		pane.getChildren().addAll(rectangle1, rectangle2);
		//Adds the pane to the vbox
		vBox.getChildren().addAll(pane, new Text(20, 0, rectangleOutput));
		//Creates a new scene
		Scene scene = new Scene(vBox);
		//Sets title
		primaryStage.setTitle("Kohl Kennedy Rectangle");
		//Sets the scene
		primaryStage.setScene(scene);
		primaryStage.show();
		//Closes the scanner
		scan.close();

	}
	//Creates a rectangle based off of the string array
	public Rectangle getRect(String[] rect) {
		Rectangle rectAngle = new Rectangle(Double.parseDouble(rect[0]), Double.parseDouble(rect[1]),
				Double.parseDouble(rect[2]), Double.parseDouble(rect[3]));
		rectAngle.setFill(Color.ANTIQUEWHITE);
		rectAngle.setStroke(Color.BLACK);
		return rectAngle;
	}

	/** Returns true if one rectangle is inside the other */
	public boolean contains(Rectangle r1, Rectangle r2) {
		return 
			r2.getY() + r2.getHeight() <= r1.getY() + r1.getHeight() && 
			r2.getX() + r2.getWidth() <= r1.getX() + r1.getWidth() && 
			r2.getX() > r1.getX() && r2.getY() > r1.getY();
	}

	/** Returns true if the specified rectangles overlap */
	public boolean overlaps(Rectangle r1, Rectangle r2) {
		return getDistance(r1.getX(), r2.getX() + r2.getWidth()) < 
			r1.getWidth() + r2.getWidth() &&
			getDistance(r1.getY(), r2.getY() + r2.getHeight()) <
			r1.getHeight() + r2.getHeight();
				 
	}

	/** Return distance */
	private double getDistance(double p1, double p2) {
		return Math.sqrt(Math.pow(p2 - p1, 2));
	}

	public static void main(String[] args) {
		launch(args);
	}

}