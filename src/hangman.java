package src;
//Imports all the necessary packages for javafx
import javafx.application.Application;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.stage.Stage;
import javafx.scene.Scene;

public class hangman extends Application{
	//Sets the size for the program
	private static double HEIGHT = 400;
	private static double WIDTH = 400;
	
	@Override
	public void start(Stage primaryStage) {
		//Creates a pane
		Pane pane = new Pane();
		//Sets the title
		primaryStage.setTitle("Kohl Kennedys Hangman Set");
		//Sets the scene for the application
		primaryStage.setScene(new Scene(pane,WIDTH,HEIGHT, Color.ALICEBLUE));
		//Creates the arc for the noose stack
		Arc baseArc = new Arc(WIDTH / 4, HEIGHT - HEIGHT / 12, WIDTH / 5, HEIGHT / 12, 0 , 180);
		//Sets the space underneath it to yellow
		baseArc.setFill(Color.YELLOW);
		//Sets the line color to black
		baseArc.setStroke(Color.BLACK);
		//Adds the holder to the pane
		pane.getChildren().add(baseArc);
		//Creates the pole
		Line pole = new Line(baseArc.getCenterX(),baseArc.getCenterY() - baseArc.getRadiusY(),baseArc.getCenterX(), pane.getHeight() / 12);
		//Adds the pole
		pane.getChildren().add(pole);
		//Creates the holder for the pole
		Line holder = new Line(pole.getEndX(),pole.getEndY(),pane.getWidth() / 1.5, pole.getEndY());
		//Adds the pole holder
		pane.getChildren().add(holder);
		//Adds the hang pole
		Line hangPole = new Line(holder.getEndX(),holder.getEndY(),holder.getEndX(),pane.getHeight() / 6);
		pane.getChildren().add(hangPole);
		//Creates the radius of the head
		double radius = WIDTH / 10;
		//Creates the head
		Circle head = new Circle(holder.getEndX(), pane.getHeight() / 6 + radius, radius);
		//Sets the head gold
		head.setFill(Color.GOLD);
		head.setStroke(Color.BLACK);
		//Adds said head
		pane.getChildren().add(head);

		//Creates the point for the left arm
		double[] p = getPoint(head, 220);
		//Creates the left arm
		Line leftArm = new Line(pane.getWidth() / 2, pane.getHeight() / 2, p[0],p[1]);
		//Adds the left arm
		pane.getChildren().add(leftArm);
		//Creates the point for the right arm
		p = getPoint(head,315);
		//Creates the right arm
		Line rightArm = new Line(pane.getWidth() / 1.2, pane.getHeight() / 2, p[0],p[1]);
		pane.getChildren().add(rightArm);
		//Gets the point at the 
		p = getPoint(head,270);
		//Creates the stick of a body
		Line body = new Line(p[0],p[1],p[0], pane.getHeight() / 1.6);
		//Left leg creation
		Line leftLeg = new Line(body.getEndX(), body.getEndY(), pane.getWidth() / 2, pane.getHeight() / 1.3);
		//Right leg creation
		Line rightLeg = new Line(body.getEndX(), body.getEndY(), pane.getWidth() / 1.2, pane.getHeight() / 1.3);
		pane.getChildren().addAll(body,leftLeg,rightLeg);
		primaryStage.show();
		
	}
	
	//This launches the program
	public static void main(String[] args) {
		launch(args);
	}
	//Created a method to get the points as a certain angle
	private double[] getPoint(Circle c, double angle) {
		//This is for the cos value
		double x =  c.getCenterX() + c.getRadius() * Math.cos(Math.toRadians(angle));
		//This is for the sin value
		double y = c.getCenterY() - c.getRadius() * Math.sin(Math.toRadians(angle));
		//Return as an array, to make it easier and more efficient
		return new double[] {x,y};
	}
}