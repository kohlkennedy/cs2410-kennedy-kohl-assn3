package src;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class circle extends Application{

    @Override
    public void start(Stage primaryStage){
        Pane pane = new Pane();
        pane.setPrefSize(250,250);
        //Sets the font
        Font font = Font.font("Times New Roman", FontWeight.EXTRA_BOLD, FontPosture.REGULAR, 35);
        //Sets the text
        String welcome = "Welcome to Java";
        //Sets the rotation
        double rotation = 90;
        //Creates the offset
        double offset = pane.getPrefWidth() / 2;
        //Creates the radius for the circle
        double radius = 100;
        //Creates the y and x values
        double x = offset + Math.cos(rotation) * radius;
        double y = offset + Math.sin(rotation) * radius;
        //For loop to offset the values
        for(int i = 0; i < welcome.length(); i++)
        {
            x = offset + Math.cos(Math.toRadians(rotation)) * radius;
            y = offset + Math.sin(Math.toRadians(rotation)) * radius;
            Text text = new Text(x,y, welcome.charAt(i) + "");

            text.setFont(font);
            text.setRotate(rotation);
            pane.getChildren().add(text);
            rotation += 22.5;
        }
        //Creates a new scene and sets it from pane
        Scene scene = new Scene(pane);
        //Makes the title and app mine
        primaryStage.setTitle("Kohl Kennedy's Circle");
        //Creates the scene
        primaryStage.setScene(scene);
        //Tells the program to show the program
        primaryStage.show();
    }

    //Launches the program
    public static void main(String[] args){
        launch(args);
    }

}